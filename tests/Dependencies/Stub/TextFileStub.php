<?php


namespace Tests\Dependencies\Stub;


use SecretSanta\Infaestructure\File\TextFile;

final class TextFileStub
{
    const INVALID_PATH = "";
    const TEXT_FILE_PATH = __DIR__ . "/../../../players.txt";

    public static function fromDisk(): TextFile
    {
        return new TextFile(self::TEXT_FILE_PATH);
    }

    public static function createInvalidFile(): TextFile
    {
        return new TextFile(self::INVALID_PATH);
    }
}