<?php


namespace Tests\Dependencies\Stub;


use SecretSanta\Domain\Model\Player\PlayersGroup;

final class PlayersGroupStub
{
    public static function create(int $playersCount): PlayersGroup
    {
        $playersGroup = PlayersGroup::create();

        while (count($playersGroup->players()) < $playersCount){
            $player = PlayerStub::random();
            if (!$playersGroup->findPlayer($player)) {
                $playersGroup->addPlayer($player);
            }
        }

        return $playersGroup;
    }

    public static function random(int $min = 2, int $max = 3)
    {
        return self::create(rand($min, $max));
    }
}

