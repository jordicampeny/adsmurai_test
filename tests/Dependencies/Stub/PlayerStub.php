<?php


namespace Tests\Dependencies\Stub;


use Faker\Factory;
use SecretSanta\Domain\Model\Player\Player;

final class PlayerStub
{
    public static function create(string $name)
    {
        return Player::fromString($name);
    }

    public static function random()
    {
        return self::create(Factory::create()->firstName);
    }
}