<?php


namespace Tests\Behaviour;


use SecretSanta\Aplication\Service\SecretSantaPlayersAssignmentUseCase;
use SecretSanta\Domain\Model\Player\PlayersGroup;
use SecretSanta\Infraestructure\Random\RandomInteger;
use Tests\Dependencies\Stub\PlayersGroupStub;
use Tests\Dependencies\TestCase\UnitTestCase;

final class SecretSantaPlayersAssignmentUseCaseTest extends UnitTestCase
{
    const TIMES_TO_TEST_ASSIGNATION_RANDOMNESS = 30;

    /** @var  SecretSantaPlayersAssignmentUseCase */
    private $usersAssignmentUserCase;

    protected function setUp()
    {
        parent::setUp();
        $this->usersAssignmentUserCase = new SecretSantaPlayersAssignmentUseCase(
            new RandomInteger()
        );
    }

    /**
     * @test
     * @dataProvider generateRandomPlayers
     * @param PlayersGroup $playersGroup
     */
    public function it_should_return_assignation_where_each_player_must_have_one_player_to_give_the_gift(
        PlayersGroup $playersGroup
    ) {
        $playersAssignationGroup            = $this->usersAssignmentUserCase->execute($playersGroup);
        $eachPlayerHaveAPlayerToGiveTheGift = true;

        //Each player has an existing name assigned
        foreach ($playersAssignationGroup->playerAssignations() as $playerAssignation) {
            if (!$playersGroup->findPlayer($playerAssignation->receiver())) {
                $eachPlayerHaveAPlayerToGiveTheGift = false;
                break;
            }
        }

        $this->assertTrue($eachPlayerHaveAPlayerToGiveTheGift);
    }

    /**
     * @test
     * @dataProvider generateRandomPlayers
     * @param PlayersGroup $playersGroup
     */
    public function it_should_return_assignation_where_each_player_must_receive_a_gift(PlayersGroup $playersGroup)
    {
        $playersAssignationGroup = $this->usersAssignmentUserCase->execute($playersGroup);
        $eachPlayerReceiveGift   = true;

        //All Players has been assigned
        if (count($playersAssignationGroup->playerAssignations()) !== count($playersGroup->players())) {
            $eachPlayerReceiveGift = false;
        }

        //Each player receive a gift
        foreach ($playersAssignationGroup->playerAssignations() as $playerAssignation) {
            if (!$playersGroup->findPlayer($playerAssignation->donor())) {
                $eachPlayerReceiveGift = false;
                break;
            }
        }

        $this->assertTrue($eachPlayerReceiveGift);
    }

    /**
     * @test
     * @dataProvider generateRandomPlayers
     * @param PlayersGroup $playersGroup
     */
    public function it_should_return_assignation_where_a_player_is_not_assigned_with_himself(PlayersGroup $playersGroup)
    {
        $playersAssignationGroup              = $this->usersAssignmentUserCase->execute($playersGroup);
        $thereAreNoPlayersAssignedWithHimself = true;

        foreach ($playersAssignationGroup->playerAssignations() as $playerAssignation) {
            if ($playerAssignation->donor()->name() === $playerAssignation->receiver()->name()) {
                $thereAreNoPlayersAssignedWithHimself = false;
                break;
            }
        }

        $this->assertTrue($thereAreNoPlayersAssignedWithHimself);
    }

    /** @test */
    public function it_should_assign_player_to_player_randomly()
    {
        $playersGroup = PlayersGroupStub::random(100, 100);

        $randomAssignationDetected = false;
        $lastAssignationGroup      = null;

        for ($i = 1; $i <= self::TIMES_TO_TEST_ASSIGNATION_RANDOMNESS; $i++) {
            $playersAssignationsGroup  = $this->usersAssignmentUserCase->execute($playersGroup);
            $randomAssignationDetected = $this->isAssignedRandomly(
                $lastAssignationGroup,
                $playersAssignationsGroup
            );

            $lastAssignationGroup = $playersAssignationsGroup;
        }

        $this->assertTrue($randomAssignationDetected);
    }

    public function generateRandomPlayers(): array
    {
        return [
            ["playersGroup" => PlayersGroupStub::random(2, 2)],
            ["playersGroup" => PlayersGroupStub::random(3, 3)],
            ["playersGroup" => PlayersGroupStub::random(2, 10)],
            ["playersGroup" => PlayersGroupStub::random(50, 100)]
        ];
    }

    /**
     * @param $lastAssignation
     * @param $playersAssignationsGroup
     * @return bool
     */
    private function isAssignedRandomly($lastAssignation, $playersAssignationsGroup): bool
    {
        return $lastAssignation && $lastAssignation != $playersAssignationsGroup;
    }
}