<?php


namespace Tests\Integration;


use Faker\Factory;
use SecretSanta\Domain\Model\File\Contracts\File;
use SecretSanta\Infraestructure\Input\Exceptions\CanNotFoundTextFileException;
use SecretSanta\Infraestructure\Input\Exceptions\InsufficientNumberOfPlayersException;
use SecretSanta\Infraestructure\Input\Exceptions\RepeatedPlayerNameException;
use SecretSanta\Infraestructure\Input\InputFromTextFile;
use Tests\Dependencies\Stub\TextFileStub;
use Tests\Dependencies\TestCase\UnitTestCase;

final class InputFromTextFileTest extends UnitTestCase
{
    private $inputFromTextFile;

    /** @test */
    public function it_should_read_list_of_players_from_text_file()
    {
        $textFile = TextFileStub::fromDisk();

        $this->assertTrue(!!$textFile->getContent());
    }

    /** @test */
    public function it_should_return_more_than_minimum_amount_of_players()
    {
        $players         = $this->getPlayersFromTextFile();
        $amountOfPlayers = count($players);

        $this->assertTrue($amountOfPlayers >= InputFromTextFile::MINIMUM_AMOUNT_OF_PLAYERS);
    }

    /** @test */
    public function it_should_return_a_collection_of_unique_players()
    {
        $players = $this->getPlayersFromTextFile();

        $this->assertTrue($this->arrayIsUnique($players));
    }

    /** @test */
    public function it_should_throw_an_exception_if_cant_found_the_file()
    {
        $this->expectException(CanNotFoundTextFileException::class);

        $invalidFile  = TextFileStub::createInvalidFile();
        $invalidInput = new InputFromTextFile($invalidFile);

        $invalidInput->read();
    }

    /** @test */
    public function it_should_throw_an_exception_if_there_are_not_enough_players()
    {
        $this->expectException(InsufficientNumberOfPlayersException::class);

        $textFile = $this->mock(File::class);
        $emptyInput = new InputFromTextFile($textFile);

        $this->shouldReturnEmptyContent($textFile);

        $emptyInput->read();
    }

    /** @test */
    public function it_should_throw_and_exception_if_there_are_repeated_players_name()
    {
        $this->expectException(RepeatedPlayerNameException::class);

        $textFile = $this->mock(File::class);
        $emptyInput = new InputFromTextFile($textFile);

        $this->shouldReturnRepeatedNamesContent($textFile);

        $emptyInput->read();
    }

    private function inputFromTextFile()
    {
        $textFile = TextFileStub::fromDisk();

        return $this->inputFromTextFile = $this->inputFromTextFile ?: new InputFromTextFile($textFile);
    }

    /**
     * @return array
     */
    private function getPlayersFromTextFile(): array
    {
        return $this->inputFromTextFile()->read();
    }

    /**
     * @param $array
     * @return bool
     */
    private function arrayIsUnique(array $array): bool
    {
        return array_unique($array) == $array;
    }

    /**
     * @param $textFile
     */
    private function shouldReturnEmptyContent($textFile)
    {
        $textFile
            ->shouldReceive('getContent')
            ->once()
            ->withNoArgs()
            ->andReturn("");
    }

    /**
     * @param $textFile
     */
    private function shouldReturnRepeatedNamesContent($textFile)
    {
        $name = Factory::create()->firstName;

        $textFile
            ->shouldReceive('getContent')
            ->once()
            ->withNoArgs()
            ->andReturn("$name\n$name");
    }
}