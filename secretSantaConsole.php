<?php

use SecretSanta\Aplication\Service\SecretSantaPlayersAssignmentUseCase;
use SecretSanta\Infaestructure\File\TextFile;
use SecretSanta\Infraestructure\Game\SecretSantaGame;
use SecretSanta\Infraestructure\Input\InputFromTextFile;
use SecretSanta\Infraestructure\Output\StandardOutput;
use SecretSanta\Infraestructure\Random\RandomInteger;

require __DIR__ . '/vendor/autoload.php';

$textFile        = new TextFile(__DIR__ . '/players.txt');
$input           = new InputFromTextFile($textFile);
$output          = new StandardOutput();
$useCase         = new SecretSantaPlayersAssignmentUseCase(
    new RandomInteger()
);

$secretSantaGame = new SecretSantaGame(
    $input,
    $output,
    $useCase
);

$secretSantaGame->play();