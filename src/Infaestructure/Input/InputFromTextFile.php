<?php


namespace SecretSanta\Infraestructure\Input;

use SecretSanta\Contracts\Input;
use SecretSanta\Domain\Model\File\Contracts\File;
use SecretSanta\Domain\Model\Player\Player;
use SecretSanta\Domain\Model\Player\PlayersGroup;
use SecretSanta\Infraestructure\Input\Exceptions\CanNotFoundTextFileException;
use SecretSanta\Infraestructure\Input\Exceptions\InsufficientNumberOfPlayersException;
use SecretSanta\Infraestructure\Input\Exceptions\RepeatedPlayerNameException;

final class InputFromTextFile implements Input
{
    const MINIMUM_AMOUNT_OF_PLAYERS = 2;

    /**
     * @var string
     */
    private $textFile;

    public function __construct(File $textFile)
    {
        $this->textFile = $textFile;
    }

    /**
     * Read the list of players from a text file
     * @return array|\string[] An array with all player names as strings
     * @throws CanNotFoundTextFileException
     * @throws InsufficientNumberOfPlayersException
     * @throws RepeatedPlayerNameException
     */
    public function read(): array
    {
        $fileContent = $this->textFile->getContent();
        $playersGroup = $this->getPlayersFromString($fileContent);

        if ($this->playersHaveDuplicates($playersGroup)) {
            throw new RepeatedPlayerNameException();
        }

        if ($this->thereAreLessThanMinimumAmountOfPlayers($playersGroup)) {
            throw new InsufficientNumberOfPlayersException();
        }

        return $playersGroup->players();
    }


    /**
     * @param PlayersGroup $playersGroup
     * @return bool
     */
    private function playersHaveDuplicates(PlayersGroup $playersGroup): bool
    {
        return array_unique($playersGroup->players()) != $playersGroup->players();
    }

    /**
     * @param $playersGroup
     * @return bool
     */
    private function thereAreLessThanMinimumAmountOfPlayers(PlayersGroup $playersGroup): bool
    {
        return count($playersGroup->players()) < self::MINIMUM_AMOUNT_OF_PLAYERS;
    }

    /**
     * @param $fileContent
     * @return PlayersGroup
     */
    private function getPlayersFromString($fileContent): PlayersGroup
    {
        $playersName  = explode("\n", $fileContent);
        $playersGroup = PlayersGroup::create();

        foreach ($playersName as $playerName) {
            $playersGroup->addPlayer(Player::fromString($playerName));
        }

        return $playersGroup;
    }
}