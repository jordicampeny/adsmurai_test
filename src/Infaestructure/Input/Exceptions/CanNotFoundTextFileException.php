<?php


namespace SecretSanta\Infraestructure\Input\Exceptions;

use Exception;
use SecretSanta\Domain\Model\Input\Contracts\Exceptions\InputError;

final class CanNotFoundTextFileException extends Exception implements InputError
{
    public function __construct(string $message = "")
    {
        parent::__construct("Text file not found");
    }
}