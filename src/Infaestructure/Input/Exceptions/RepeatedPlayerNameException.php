<?php


namespace SecretSanta\Infraestructure\Input\Exceptions;

use Exception;
use SecretSanta\Domain\Model\Input\Contracts\Exceptions\InputError;

final class RepeatedPlayerNameException extends Exception implements InputError
{
    public function __construct(string $message = "")
    {
        parent::__construct("There is a player with a repeated name");
    }
}