<?php


namespace SecretSanta\Infraestructure\Output;


use SecretSanta\Contracts\Output;

final class StandardOutput implements Output
{
    /**
     * @param string $line
     */
    public function printLine(string $line)
    {
        echo $line;
    }
}