<?php


namespace SecretSanta\Infraestructure\Game;


use SecretSanta\Contracts\Game;
use SecretSanta\Contracts\Input;
use SecretSanta\Contracts\Output;
use SecretSanta\Aplication\Service\SecretSantaPlayersAssignmentUseCase;
use SecretSanta\Domain\Model\Player\PlayersGroup;

final class SecretSantaGame implements Game
{
    /** @var Input */
    private $input;
    /** @var Output */
    private $output;
    /** @var SecretSantaPlayersAssignmentUseCase */
    private $usersAssignmentUseCase;

    /**
     * SecretSantaGame constructor.
     * @param Input $input
     * @param Output $output
     * @param SecretSantaPlayersAssignmentUseCase $usersAssignmentUseCase
     */
    public function __construct(
        Input $input,
        Output $output,
        SecretSantaPlayersAssignmentUseCase $usersAssignmentUseCase
    ) {
        $this->input                  = $input;
        $this->output                 = $output;
        $this->usersAssignmentUseCase = $usersAssignmentUseCase;
    }

    public function play()
    {
        $playersGroup    = PlayersGroup::create($this->input->read());
        $assignedPlayers = $this->usersAssignmentUseCase->execute($playersGroup);
        $this->output->printLine($assignedPlayers);
    }
}