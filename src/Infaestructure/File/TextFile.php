<?php


namespace SecretSanta\Infaestructure\File;


use SecretSanta\Domain\Model\File\Contracts\File;
use SecretSanta\Infraestructure\Input\Exceptions\CanNotFoundTextFileException;

final class TextFile implements File
{
    /** @var string */
    private $path;

    /**
     * TextFile constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     * @throws CanNotFoundTextFileException
     */
    public function getContent(): string
    {
        if (!file_exists($this->path)) {
            throw new CanNotFoundTextFileException();
        }

        return file_get_contents($this->path);
    }
}