<?php


namespace SecretSanta\Infraestructure\Random;


use SecretSanta\Contracts\Random;

final class RandomInteger implements Random
{

    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    public function generate(int $min, int $max): int
    {
        return rand($min, $max);
    }
}