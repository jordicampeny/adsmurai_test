<?php


namespace SecretSanta\Aplication\Service;


use SecretSanta\Contracts\Random;
use SecretSanta\Domain\Model\Player\Player;
use SecretSanta\Domain\Model\Player\PlayerAssignation;
use SecretSanta\Domain\Model\Player\PlayersAssignationGroup;
use SecretSanta\Domain\Model\Player\PlayersGroup;

final class SecretSantaPlayersAssignmentUseCase
{
    /** @var Random */
    private $random;
    /** @var  PlayersGroup */
    private $playersToAssign;
    /** @var  PlayersGroup */
    private $unAssignedPlayers;
    /** @var  PlayersAssignationGroup */
    private $assignedPlayers;

    /**
     * SecretSantaPlayersAssignmentUseCase constructor.
     * @param Random $random
     */
    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    /**
     * @param PlayersGroup $playersGroup
     * @return PlayersAssignationGroup
     * @internal param array $players
     */
    public function execute(PlayersGroup $playersGroup): PlayersAssignationGroup
    {
        $this->playersToAssign   = PlayersGroup::create($playersGroup->players());
        $this->unAssignedPlayers = PlayersGroup::create($playersGroup->players());
        $this->assignedPlayers   = PlayersAssignationGroup::create();

        foreach ($this->playersToAssign->players() as $player) {
            $this->findRandomPlayerToAssign($player);
        }

        return $this->assignedPlayers;
    }

    /**
     * @param $randomPlayer
     * @param $player
     */
    private function assignPlayerToPlayer(Player $randomPlayer, Player $player)
    {
        $this->unAssignedPlayers->removePlayer($randomPlayer);
        $playerAssignation = PlayerAssignation::create(
            $player,
            $randomPlayer
        );
        $this->assignedPlayers->addAssignations($playerAssignation);
    }

    /**
     * @param Player $player
     * @param Player $playerToAssign
     * @return bool
     */
    private function onePlayerIsMissingToAssignAndHaveNoPair(Player $player, Player $playerToAssign): bool
    {
        return count($this->unAssignedPlayers->players()) === 1 && $player->isCalledSameAs($playerToAssign);
    }

    /**
     * @param $player
     * @internal param array $players
     */
    private function findRandomPlayerToAssign(Player $player)
    {
        $assigned = false;

        while (!$assigned) {
            $randomPlayer = $this->unAssignedPlayers->getRandomPlayer($this->random);

            if (!$player->isCalledSameAs($randomPlayer)) {
                $this->assignPlayerToPlayer($randomPlayer, $player);
                $assigned = true;
            }

            if ($this->onePlayerIsMissingToAssignAndHaveNoPair($player, $randomPlayer)) {
                $playerSwapped = $this->assignedPlayers->swapReceiver($player);
                $this->assignPlayerToPlayer($playerSwapped, $player);
                $assigned = true;
            }
        }
    }
}