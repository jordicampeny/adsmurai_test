<?php


namespace SecretSanta\Domain\Model\Input\Contracts\Exceptions;

use Throwable;

interface InputError extends Throwable
{

}