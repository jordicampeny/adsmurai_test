<?php


namespace SecretSanta\Domain\Model\File\Contracts;


interface File
{
    public function getContent(): string;
}