<?php


namespace SecretSanta\Domain\Model\Player;


use Doctrine\Instantiator\Exception\InvalidArgumentException;

final class PlayersAssignationGroup
{
    private $playerAssignations;

    /**
     * PlayersAssignationGroup constructor.
     * @param array $playersAssignation
     */
    private function __construct(array $playersAssignation = [])
    {
        $this->playerAssignations = $playersAssignation;
    }

    /**
     * @return PlayersAssignationGroup
     */
    public static function create(): PlayersAssignationGroup
    {
        return new self();
    }

    /**
     * @param array $playersAssignations
     * @return PlayersAssignationGroup
     */
    public static function fromArray(array $playersAssignations): PlayersAssignationGroup
    {
        return new self($playersAssignations);
    }

    /**
     * @return array
     */
    public function playerAssignations(): array
    {
        return $this->playerAssignations;
    }

    /**
     * @param PlayerAssignation $playerAssignation
     * @return PlayersAssignationGroup
     */
    public function addAssignations(PlayerAssignation $playerAssignation): PlayersAssignationGroup
    {
        $this->playerAssignations[] = $playerAssignation;
        return $this;
    }

    /**
     * @param Player $player
     * @param int $fromPosition
     * @return Player
     */
    public function swapReceiver(Player $player, int $fromPosition = 0): Player
    {
        if (!$this->playerAssignations[$fromPosition]) {
            throw new InvalidArgumentException("Swap assignations doesn't exist to position $fromPosition");
        }

        $assignationToBeSwap = $this->playerAssignations[$fromPosition];
        $receiverSwapped     = $assignationToBeSwap->receiver();
        $assignationToBeSwap->setReceiver($player);

        return $receiverSwapped;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $stringLine = "";

        foreach ($this->playerAssignations as $playerAssignation) {
            $stringLine .= $playerAssignation->donor() . " => " . $playerAssignation->receiver() . "\n";
        }

        return $stringLine;
    }
}