<?php


namespace SecretSanta\Domain\Model\Player;


final class Player
{
    private $name;

    /**
     * Player constructor.
     * @param string $name
     */
    private function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     * @return Player
     */
    public static function fromString(string $name): Player
    {
        return new self($name);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @param Player $player
     * @return bool
     */
    public function isCalledSameAs(Player $player): bool
    {
        return $this->name === $player->name();
    }
}