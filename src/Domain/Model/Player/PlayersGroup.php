<?php


namespace SecretSanta\Domain\Model\Player;


use SecretSanta\Contracts\Random;

final class PlayersGroup
{
    private $players;

    /**
     * PlayersGroup constructor.
     * @param array $players
     */
    private function __construct(array $players = [])
    {
        $this->players = $players;
    }

    /**
     * @param array $players
     * @return PlayersGroup
     */
    public static function create(array $players = []): PlayersGroup
    {
        return new self($players);
    }

    /**
     * @param Random $randomGenerator
     * @return Player
     */
    public function getRandomPlayer(Random $randomGenerator): Player
    {
        $randomChoice = $randomGenerator->generate(0, count($this->players) - 1);

        return $this->players[$randomChoice];
    }

    /**
     * @param Player $player
     * @return bool
     */
    public function findPlayer(Player $player): bool
    {
        return in_array($player, $this->players);
    }

    /**
     * @param Player $player
     * @return PlayersGroup
     */
    public function addPlayer(Player $player): PlayersGroup
    {
        if ($player->name()) {
            $this->players[] = $player;
        }

        return $this;
    }

    /**
     * @param Player $player
     * @return PlayersGroup
     */
    public function removePlayer(Player $player): PlayersGroup
    {
        $indexPlayer = array_search($player, $this->players);
        array_splice($this->players, $indexPlayer, 1);

        return $this;
    }

    /**
     * @return array
     */
    public function players(): array
    {
        return $this->players;
    }
}