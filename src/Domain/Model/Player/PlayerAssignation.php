<?php


namespace SecretSanta\Domain\Model\Player;


final class PlayerAssignation
{
    /** @var Player */
    private $donor;
    /** @var Player */
    private $receiver;

    /**
     * PlayerAssignation constructor.
     * @param Player $donor
     * @param Player $receiver
     */
    private function __construct(Player $donor, Player $receiver)
    {
        $this->donor    = $donor;
        $this->receiver = $receiver;
    }

    /**
     * @param Player $donor
     * @param Player $receiver
     * @return PlayerAssignation
     */
    public static function create(Player $donor, Player $receiver): PlayerAssignation
    {
        return new self($donor, $receiver);
    }

    /**
     * @param Player $donor
     */
    public function setDonor(Player $donor)
    {
        $this->donor = $donor;
    }

    /**
     * @param Player $receiver
     */
    public function setReceiver(Player $receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return Player
     */
    public function donor(): Player
    {
        return $this->donor;
    }

    /**
     * @return Player
     */
    public function receiver(): Player
    {
        return $this->receiver;
    }
}